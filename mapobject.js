function mapObject(obj,cb){
    if(!obj || !cb){
        return null
    }else{
        let newobj = {};
        for (let keys in obj){
        newobj[keys]=((cb(keys,obj[keys])));
        }return newobj
    }
}

module.exports = mapObject; 
